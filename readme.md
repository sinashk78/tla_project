# TLA Project

Languages:
-
#### First language:
𝐿 = {𝑤 | 𝑤 ∊ {𝑎, 𝑏, 𝑐}∗ , len(w) % 2 == 0}

![𝐿 = {𝑤 | 𝑤 ∊ {𝑎, 𝑏, 𝑐}∗ , len(w) % 2 == 0}](res/L1.png)
-
#### Second language:
𝐿 = {𝑤 | 𝑤 ∊ {𝑎, 𝑏, 𝑐}∗ , (𝑛𝑎 (𝑤) − 𝑛𝑏 (𝑤) % 3 == 1}

![𝐿 = {𝑤 | 𝑤 ∊ {𝑎, 𝑏, 𝑐}∗ , (𝑛𝑎 (𝑤) − 𝑛𝑏 (𝑤) % 3 == 1}](res/L2.png)
-
#### Third language:
𝐿 = {𝑤𝑐𝑤<sup>𝑟</sup> | 𝑤 ∊ {𝑎, 𝑏, 𝑐} ∗ }

![𝐿 = {𝑤𝑐𝑤<sup>𝑟</sup> | 𝑤 ∊ {𝑎, 𝑏, 𝑐} ∗ }](res/L3.png)
-
#### Fourth language:
𝐿 = {𝑎<sup>𝑛</sup>𝑏<sup>𝑛+𝑚</sup>𝑎<sup>𝑚</sup> | 𝑚, 𝑛 ≥ 1}

![𝐿 = {𝑎<sup>𝑛</sup>𝑏<sup>𝑛+𝑚</sup>𝑎<sup>𝑚</sup> | 𝑚, 𝑛 ≥ 1}](res/L4.png)
-
#### Fifth language:
𝐿 = { 𝑤𝑤 | 𝑤 ∊ {𝑎, 𝑏, 𝑐} .5 ∗ }

![𝐿 = { 𝑤𝑤 | 𝑤 ∊ {𝑎, 𝑏, 𝑐} .5 ∗ }](res/L5.png)

## Functions: 
* traverse(String): traversing the input string
* handleTransition(State, char): handles the transition from ```state``` to the state got from ```char``` if exists
* checkStatus(State): checks if the machine accepts the language or not
* handlePop(Transition ) : handles pop in PDA for language 3
* handlePush(Transition)  : handles push in PDA for language 3
* GetNewModel(State, Transition) adds the transition and ```from``` and ```to``` states * with stack operation to a queue so later if the input is accepted we can show what were the steps for this input.
* read() returns current symbol that the tape head's is at.
* write(Alphabet) writes ```Alphabet``` to the tape where at position ```head```


Usage:
-
Run ```main``` function in ```Main``` class.
Enter the language number or ```h``` for help when you see ```Enter Command: ```
After you select your language enter the desired text and if the machine accepts it, it will print the steps and if it doesn't. it simply says ```doesn't accept```

## Notes
* I started from the first language simple and easy and added more complexity if needed for next languages so the code for each language may be different.
* For the first and second language i stuck with ```enums``` which had a flag called is final and since its a ```DFA``` i added hard coded all the transition since they weren't gonna change.
* For the third language i change the code and added a ```Transition``` which is just a ```POJO``` that contains destination and the pop and push letters from stack and treat this language as ```D PDA```. basically i didn't need the ability to pop or push multiple elements from the stack and i didn't have multiple transitions for the same letter.
* For the fourth language i change every thing so it could handle multiple pop and push per transition and multiple transitions per letter.
* For fifth language i created new model ```TuringModel``` for showing the transitions and replaced ```LAMBDA``` with ```$``` for easier use and updated the ```Transition``` so it could handle ```read```,```write```, and tape commands such as ```L``` and ```R```.
* Each ```State``` has a map of transitions so no duplicate transitions are allowed!
* After creating the machine i will add ```&``` as empty character to both end and beginning of the input and an integer ```head``` which is equal to 1 at first.
* The machine has 2 methods called read and write to handle read/write operations.
* For further explanation i believe we have a presentation so i will explain everything in details there.
* This project uses ```Java 8```
## Gitlab
* https://gitlab.com/sinashk78/tla_project
### Thank You 
##### Sina Shabani Kumeleh