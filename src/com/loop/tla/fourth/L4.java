package com.loop.tla.fourth;

import com.loop.tla.base.PDAModel;
import com.loop.tla.third.L3;

import java.util.*;

public class L4 {
    private Stack<Alphabet> stack = new Stack<>();
    private Queue<PDAModel> queue = new LinkedList<>();
    private static Map<Character , Alphabet> map = new HashMap<>();

    static {
        map.put('a' , Alphabet.a);
        map.put('b' , Alphabet.b);
    }

    static class Transition{
        Alphabet name;
        State to;
        List<Alphabet> push , pop;

        public Transition(State to, List<Alphabet> pop, List<Alphabet> push , Alphabet name) {
            this.to = to;
            this.push = push;
            this.pop = pop;
            this.name = name;
        }
    }

    private enum  Alphabet{
        a,b,LAMBDA,$
    }
    private class State{

        final boolean isFinal;
        final String name;
        State(boolean isFinal , String name){
            this.isFinal = isFinal;
            this.name = name;
        }

        // transitions
        LinkedList<Transition> transitions = new LinkedList<>();

        void failed(Transition transition){
            transitions.remove(transition);
        }

        Transition transition(Alphabet alphabet){

            Transition transition = transitions.stream()
                    .filter(x -> x.name == alphabet)
                    .findFirst()
                    .orElse(null);

            return transition;
        }
    }

    State Q0 , Q1 , Q2 , Q3 , Q4 , Q5;

    public L4(){
        Q0 = new State(false, "Q0");
        Q1 = new State(false, "Q1");
        Q2 = new State(false, "Q2");
        Q3 = new State(false, "Q3");
        Q4 = new State(false, "Q4");
        Q5 = new State(true, "Q5");

        Q0.transitions.add(new Transition(Q1 , Collections.singletonList(Alphabet.LAMBDA), Collections.singletonList(Alphabet.$), Alphabet.LAMBDA));
//                                                  pop me                                                  push me
        Q1.transitions.add(new Transition(Q1 , Collections.singletonList(Alphabet.LAMBDA) , Collections.singletonList(Alphabet.a), Alphabet.a));
        Q1.transitions.add(new Transition(Q2 , Collections.singletonList(Alphabet.a) , Collections.singletonList(Alphabet.LAMBDA), Alphabet.b));

        Q2.transitions.add(new Transition(Q2 , Collections.singletonList(Alphabet.a) , Collections.singletonList(Alphabet.LAMBDA) , Alphabet.b));
        Q2.transitions.add(new Transition(Q3 , Collections.singletonList(Alphabet.$) , Arrays.asList(Alphabet.$ , Alphabet.b) , Alphabet.b));

        Q3.transitions.add(new Transition(Q3 , Collections.singletonList(Alphabet.LAMBDA) , Collections.singletonList(Alphabet.b), Alphabet.b));
        Q3.transitions.add(new Transition(Q4 , Collections.singletonList(Alphabet.b) , Collections.singletonList(Alphabet.LAMBDA), Alphabet.a));

        Q4.transitions.add(new Transition(Q4 , Collections.singletonList(Alphabet.b) , Collections.singletonList(Alphabet.LAMBDA), Alphabet.a));
        Q4.transitions.add(new Transition(Q5 , Collections.singletonList(Alphabet.$) , Collections.singletonList(Alphabet.LAMBDA), Alphabet.LAMBDA));
    }



    public void traverse(String input) throws IllegalArgumentException{
        State state = Q0;
        Transition transition = state.transitions.get(0);
        handlePush(transition);
        queue.add(GetNewModel(state , transition));
        state = transition.to;
        for (int i = 0; i < input.length(); i++) {
            char ch = input.charAt(i);
            Alphabet alphabet = map.get(ch);
            if(alphabet == null)
                throw new IllegalArgumentException("Wrong Alphabet: " + ch);
            transition = state.transition(alphabet);
            if(transition == null) break;
            if(!handlePop(transition))
            {
                if(!state.transitions.isEmpty()){
                    i--;
                    state.failed(transition);
                    continue;
                }
                break;
            }
            handlePush(transition);
            queue.add(GetNewModel(state , transition));
            state = transition.to;
        }

        Alphabet alphabet = stack.pop();

        if(alphabet == Alphabet.$)
        {
            transition = state.transition(Alphabet.LAMBDA);
            if(transition != null){
                queue.add(GetNewModel(state , transition));
                state = transition.to;
            }
        }

        if(state.isFinal && alphabet == Alphabet.$){
            System.out.println("accepts");
            while (!queue.isEmpty()){
                System.out.println(queue.poll().toString());
            }
            stack.clear();
        }
        else{
            queue.clear();
            stack.clear();
            System.out.println("doesnt accept");
        }
    }


    private boolean handlePop(Transition transition){
        Alphabet alphabet;

        for (int i = 0; i < transition.pop.size(); i++) {
            if(transition.pop.get(i) != Alphabet.LAMBDA){
                alphabet = stack.peek();
            }
            else continue;
            if(alphabet == transition.pop.get(i))
                stack.pop();
            else return false;
        }
        return true;
    }
    private void handlePush(Transition transition){
        for (int i = 0; i < transition.push.size(); i++) {
            if(transition.push.get(i) != Alphabet.LAMBDA)
                stack.push(transition.push.get(i));

        }
    }

    private static PDAModel GetNewModel(State state , Transition transition){
        PDAModel model = new PDAModel();
        model.from = state.name;
        model.to = transition.to.name;
        model.transition = transition.name.name();
        StringBuilder pushed = new StringBuilder();
        StringBuilder popped = new StringBuilder();
        transition.push.forEach(x -> pushed.append(x.name()));
        transition.pop.forEach(x -> popped.append(x.name()));
        model.pushed = pushed.toString();
        model.popped = popped.toString();
        return model;
    }
}
