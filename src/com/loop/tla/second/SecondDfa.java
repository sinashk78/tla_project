package com.loop.tla.second;

import com.loop.tla.base.TransitionModel;

import java.util.*;

public class SecondDfa {
    private enum  Alphabet{
        a,b,c
    }
    private enum State{
        Q0(false) , Q1(true) , Q2(false);
        final boolean isFinal;
        State(boolean isFinal){
            this.isFinal = isFinal;
        }

        // transitions
        static {
            Q0.a = Q1; Q0.b = Q2; Q0.c = Q0;
            Q1.a = Q2; Q1.b = Q0; Q1.c = Q1;
            Q2.a = Q0; Q2.b = Q1; Q2.c = Q2;
        }



        // transitions
        State a , b ,c;

        State transition(Alphabet alphabet){
            switch (alphabet){
                case a:
                    return this.a;
                case b:
                    return this.b;
                case c:
                    return this.c;
                default:
                    throw new IllegalArgumentException("Wrong alphabet!");
            }
        }
    }
    private final static Map<Character , Alphabet> map = new HashMap<>();

    private final Queue<TransitionModel> transitionModels = new LinkedList<>();
    static {
        map.put('a' , Alphabet.a);
        map.put('b' , Alphabet.b);
        map.put('c' , Alphabet.c);
    }

    public SecondDfa(){}

    public void traverse(String input) throws IllegalArgumentException{
        State state = State.Q0;
        for (int i = 0; i < input.length(); i++) {
            String from = state.name();
            char transition = input.charAt(i);
            Alphabet alphabet = map.get(transition);
            if(alphabet == null)
                throw new IllegalArgumentException("Wrong alphabet: "+ transition);
            state = state.transition(alphabet);
            String to = state.name();
            transitionModels.add(new TransitionModel(from , to , transition));
        }
        checkStatus(state);
    }

    private void checkStatus(State state) {
        if(state.isFinal){
            System.out.println("accepts");
            while (!transitionModels.isEmpty()){
                TransitionModel tm = transitionModels.poll();
                System.out.println(tm.toString());
            }
        }
        else{
            transitionModels.clear();
            System.out.println("doesnt accept");
        }
    }


}
