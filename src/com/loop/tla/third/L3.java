package com.loop.tla.third;

import com.loop.tla.base.PDAModel;

import java.util.*;

public class L3 {
    private  Stack<Alphabet> stack = new Stack<>();
    private  Queue<PDAModel> queue = new LinkedList<>();
    private static Map<Character , Alphabet> map = new HashMap<>();

    static {
        map.put('a' , Alphabet.a);
        map.put('b' , Alphabet.b);
        map.put('c' , Alphabet.c);
    }

    static class Transition{
        String name;
        State to;
        Alphabet push , pop;

        public Transition(State to, Alphabet push, Alphabet pop , String name) {
            this.to = to;
            this.push = push;
            this.pop = pop;
            this.name = name;
        }
    }

    private enum  Alphabet{
        a,b,c,LAMBDA,$
    }
    private enum State{
        Q0(false) , Q1(false) , Q2(false) , Q3(true);
        final boolean isFinal;
        State(boolean isFinal){
            this.isFinal = isFinal;
        }

        // transitions
        static {
            Q0.lambda = new Transition(Q1 , Alphabet.$ , Alphabet.LAMBDA , "LAMBDA");
//                                      push me      pop me
            Q1.a = new Transition(Q1 , Alphabet.a , Alphabet.LAMBDA, "a");
            Q1.b = new Transition(Q1 , Alphabet.b , Alphabet.LAMBDA, "b");
            Q1.c = new Transition(Q2 , Alphabet.LAMBDA , Alphabet.LAMBDA, "c");

            Q2.a = new Transition(Q2 ,Alphabet.LAMBDA , Alphabet.a, "a");
            Q2.b = new Transition(Q2 , Alphabet.LAMBDA ,Alphabet.b , "b");
            Q2.lambda = new Transition(Q3 , Alphabet.LAMBDA , Alphabet.$, "LAMBDA");
        }



        // transitions
        Transition a , b, c,lambda;

        Transition transition(Alphabet alphabet){
            switch (alphabet){
                case a:
                    return this.a;
                case b:
                    return this.b;
                case c:
                    return this.c;
                case LAMBDA:
                    return this.lambda;
                default:
                    throw new IllegalArgumentException("Wrong alphabet!");
            }
        }
    }

    public L3(){}

    public void traverse(String input) throws IllegalArgumentException{
        State state = State.Q0;
        Transition transition = state.lambda;
        handlePush(transition);
        queue.add(GetNewModel(state , transition));
        state = transition.to;
        boolean failed = false;
        for (int i = 0; i < input.length(); i++) {
            char ch = input.charAt(i);
            Alphabet alphabet = map.get(ch);
            if(alphabet == null)
                throw new IllegalArgumentException("Wrong Alphabet: " + ch);
            transition = state.transition(alphabet);
            if(transition == null) break;
            if(!handlePop(transition))
            {
                failed = true;
                break;
            }
            handlePush(transition);
            queue.add(GetNewModel(state , transition));
            state = transition.to;
        }

        Alphabet alphabet = stack.pop();

        if(alphabet == Alphabet.$ && !failed)
        {
            transition = state.lambda;
            if(transition != null){
                queue.add(GetNewModel(state , transition));
                state = transition.to;
            }
        }

        if(state.isFinal && alphabet == Alphabet.$){
            System.out.println("accepts");
            while (!queue.isEmpty()){
                System.out.println(queue.poll().toString());
            }
        }
        else{
            queue.clear();
            stack.clear();
            System.out.println("doesnt accept");
        }

    }

    private boolean handlePop(Transition transition){
        Alphabet alphabet;
        if(transition.pop != Alphabet.LAMBDA){
            alphabet = stack.peek();
        }
        else return true;
        if(alphabet == transition.pop)
        {
            stack.pop();
            return true;
        }
        return false;
    }
    private void handlePush(Transition transition){
        if(transition.push != Alphabet.LAMBDA)
        {
            stack.push(transition.push);
        }
    }

    private static PDAModel GetNewModel(State state , Transition transition){
        PDAModel model = new PDAModel();
        model.from = state.name();
        model.to = transition.to.name();
        model.transition = transition.name;
        model.pushed = transition.push.name();
        model.popped = transition.pop.name();
        return model;
    }

}
