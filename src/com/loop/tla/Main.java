package com.loop.tla;

import com.loop.tla.fifth.L5;
import com.loop.tla.first.FirstDfa;
import com.loop.tla.fourth.L4;
import com.loop.tla.second.SecondDfa;
import com.loop.tla.third.L3;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Main {
    private enum Command{
        L1 , L2 , L3 , L4 , L5 , H
    }
    private static Map<String , Command> map = new HashMap<>();
    private static Map<Command , String> languages = new HashMap<>();
    static {
        map.put("1", Command.L1);
        map.put("2", Command.L2);
        map.put("3", Command.L3);
        map.put("4", Command.L4);
        map.put("5", Command.L5);
        map.put("h", Command.H);
        languages.put(Command.L1, "{\uD835\uDC64 | \uD835\uDC64 ∊ {\uD835\uDC4E, \uD835\uDC4F, \uD835\uDC50}∗, len(w) % 2 == 0}");
        languages.put(Command.L2, "{\uD835\uDC64 | \uD835\uDC64 ∊ {\uD835\uDC4E, \uD835\uDC4F, \uD835\uDC50}∗,(\uD835\uDC5B\uD835\uDC4E(\uD835\uDC64) − \uD835\uDC5B\uD835\uDC4F(\uD835\uDC64) % 3 == 1}");
        languages.put(Command.L3, "{\uD835\uDC64\uD835\uDC50\uD835\uDC64^\uD835\uDC5F | \uD835\uDC64 ∊ {\uD835\uDC4E, \uD835\uDC4F}∗}");
        languages.put(Command.L4, "{\uD835\uDC4E^\uD835\uDC5B\uD835\uDC4F^(\uD835\uDC5B+\uD835\uDC5A)\uD835\uDC4E^\uD835\uDC5A | \uD835\uDC5A, \uD835\uDC5B ≥ 1}");
        languages.put(Command.L5, "{ \uD835\uDC64\uD835\uDC64 | \uD835\uDC64 ∊ {\uD835\uDC4E, \uD835\uDC4F, \uD835\uDC50}∗}");
    }
    public static void main(String[] args) {

        System.out.println("type 'h' for help");

        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter a Command: ");

        while (scanner.hasNextLine()){
            String commandStr = scanner.nextLine().toLowerCase();
            if(map.containsKey(commandStr))
            {
                Command command = map.get(commandStr);
                String language = languages.get(command) + "\ninput: ";
                try {
                    switch (command) {
                        case L1:
                            System.out.print(language);
                            new FirstDfa().traverse(scanner.nextLine());
                            break;
                        case L2:
                            System.out.print(language);
                            new SecondDfa().traverse(scanner.nextLine());
                            break;
                        case L3:
                            System.out.print(language);
                            new L3().traverse(scanner.nextLine());
                            break;
                        case L4:
                            System.out.print(language);
                            new L4().traverse(scanner.nextLine());
                            break;
                        case L5:
                            System.out.print(language);
                            new L5(scanner.nextLine()).traverse();
                            break;

                        case H:
                            System.out.println(Help());
                            break;
                    }
                }
                catch(Exception ex){
                    System.err.println(ex.getMessage()); }

                try {
                    Thread.sleep(50);
                    System.out.print("Enter a Command: ");
                }catch (Exception ex){
                    // ignored
                }
            }
            else
            {
                System.err.println("Wrong Language!");
                try {
                    Thread.sleep(50);
                    System.out.print("Enter a Command: ");
                }catch (Exception ex){
                    // ignored
                }
            }

        }
        scanner.close();

    }

    private static String Help(){
        return "type the text inside single quotes...\n" +
                "Language 1: '1': L= "+ languages.get(Command.L1)+ "\n" +
                "Language 2: '2': L= "+ languages.get(Command.L2)+ "\n" +
                "Language 3: '3': L= "+ languages.get(Command.L3)+ "\n" +
                "Language 4: '4': L= "+ languages.get(Command.L4)+ "\n" +
                "Language 5: '5': L= "+ languages.get(Command.L5)+ "";
    }
}
