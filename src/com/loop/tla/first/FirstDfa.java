package com.loop.tla.first;

import com.loop.tla.base.TransitionModel;

import java.util.*;

public class FirstDfa {

    private enum  Alphabet{
        a,b,c
    }
    private enum State{
        Q0(true) , Q1(false);
        final boolean isFinal;
        State(boolean isFinal){
            this.isFinal = isFinal;
        }

        static {
            Q0.a = Q1; Q0.b = Q1; Q0.c = Q1;
            Q1.a = Q0; Q1.b = Q0; Q1.c = Q0;
        }
        // transitions
        State a , b ,c;

        State transition(Alphabet alphabet){
            switch (alphabet){
                case a:
                    return this.a;
                case b:
                    return this.b;
                case c:
                    return this.c;
                default:
                    throw new IllegalArgumentException("Wrong alphabet!");
            }
        }
    }

    private final static Map<Character , Alphabet> map = new HashMap<>();
    private final Queue<TransitionModel> transitionModels = new LinkedList<>();
    static {
        map.put('a' , Alphabet.a);
        map.put('b' , Alphabet.b);
        map.put('c' , Alphabet.c);
    }
    public FirstDfa(){ }

    public void traverse(String input) throws IllegalArgumentException {
        State state = State.Q0;
        for (int i = 0; i < input.length(); i++) {
            state = handleTransition(state , input.charAt(i));
        }
        checkStatus(state);
    }

    private State handleTransition(State state , char ch) throws IllegalArgumentException{
        String from = state.name();
        Alphabet alphabet = map.get(ch);
        if(alphabet == null)
            throw new IllegalArgumentException("Wrong alphabet: " + ch);
        State newState = state.transition(alphabet);
        String to = newState.name();
        transitionModels.add(new TransitionModel(from , to , ch));
        return newState;
    }

    private void checkStatus(State state) {
        if(state.isFinal){
            System.out.println("accepts");
            while (!transitionModels.isEmpty()){
                TransitionModel tm = transitionModels.poll();
                System.out.println(tm.toString());
            }
        }
        else{
            transitionModels.clear();
            System.out.println("doesnt accept");
        }
    }

}


