package com.loop.tla.base;

public class PDAModel {
    public String from;
    public String to;
    public String transition;
    public String pushed;
    public String popped;

    @Override
    public String toString() {
        return from + "  ==== (" +
                transition +","+popped +" -> " + pushed
                + ") ====> " + to;
    }
}
