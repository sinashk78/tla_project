package com.loop.tla.base;

public class TuringModel {
    String from , to , read , write , tapeCommand;

    public TuringModel(String from, String to, String read, String write, String tapeCommand) {
        this.from = from;
        this.to = to;
        this.read = read;
        this.write = write;
        this.tapeCommand = tapeCommand;
    }

    @Override
    public String toString() {
        return from + " ==== " + read + " -> " + write + " , " + tapeCommand + " ====> " + to;
    }
}
