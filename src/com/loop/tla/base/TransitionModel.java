package com.loop.tla.base;

public class TransitionModel {
    public String from;
    public String to;
    public char transition;

    public TransitionModel(String from, String to, char transition) {
        this.from = from;
        this.to = to;
        this.transition = transition;
    }

    @Override
    public String toString() {
        return from + " ==== " + transition + " ====> " + to;
    }
}
