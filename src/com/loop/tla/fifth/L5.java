package com.loop.tla.fifth;

import com.loop.tla.base.TuringModel;

import java.util.*;

public class L5 {
    private Queue<TuringModel> queue = new LinkedList<>();
    private static Map<Character , Alphabet> map = new HashMap<>();

    static {
        map.put('a' , Alphabet.a);
        map.put('b' , Alphabet.b);
        map.put('c' , Alphabet.c);
        map.put('A' , Alphabet.A);
        map.put('B' , Alphabet.B);
        map.put('C' , Alphabet.C);
        map.put('$' , Alphabet.$);
        map.put('R' , Alphabet.R);
        map.put('L' , Alphabet.L);
    }

    static class Transition{
        Alphabet read , write , tapeCommand;
        State to;

        public Transition(State to, Alphabet read, Alphabet write, Alphabet tapeCommand) {
            this.read = read;
            this.write = write;
            this.tapeCommand = tapeCommand;
            this.to = to;
        }
    }

    private enum  Alphabet{
        a,b,c,A,B,C, $,R,L
    }
    private class State{

        final boolean isFinal;
        final String name;
        State(boolean isFinal , String name){
            this.isFinal = isFinal;
            this.name = name;
        }

        // transitions
       Map<Alphabet , Transition> transitions = new HashMap<>();

        Transition transition(Alphabet alphabet){

            if(transitions.containsKey(alphabet))
                return transitions.get(alphabet);
            return null;
        }
    }


    String tape;
    int head = 1;
    State Q0 , Q1 , Q2 , Q3, Q4 , Q5 , Q6 , Q7 , Q8 , Q9 , Q10 , Q11 , Q12 , Q13 , Q14 , Q15 , Q16 , Q17;
    public L5(String input){
        // $ is empty character
        tape = "$" + input + "$";
        Q0 = new State(false , "Q0");
        Q1 = new State(false , "Q1");
        Q2 = new State(false , "Q2");
        Q3 = new State(false , "Q3");
        Q4 = new State(false , "Q4");
        Q5 = new State(false , "Q5");
        Q6 = new State(false , "Q6");
        Q7 = new State(false , "Q7");
        Q8 = new State(false , "Q8");
        Q9 = new State(false , "Q9");
        Q10 = new State(false , "Q10");
        Q11 = new State(false , "Q11");
        Q12 = new State(false , "Q12");
        Q13 = new State(false , "Q13");
        Q14 = new State(false , "Q14");
        Q15 = new State(false , "Q15");
        Q16 = new State(false , "Q16");
        Q17 = new State(true , "Q17");
                                                    //to   //read    /write      // tape command
        Q0.transitions.put(Alphabet.a , new Transition(Q1,Alphabet.a,Alphabet.A , Alphabet.R));
        Q0.transitions.put(Alphabet.b , new Transition(Q1,Alphabet.b,Alphabet.B , Alphabet.R));
        Q0.transitions.put(Alphabet.c , new Transition(Q1,Alphabet.c,Alphabet.C , Alphabet.R));
        Q0.transitions.put(Alphabet.A , new Transition(Q4,Alphabet.A,Alphabet.A , Alphabet.L));
        Q0.transitions.put(Alphabet.B , new Transition(Q4,Alphabet.B,Alphabet.B , Alphabet.L));
        Q0.transitions.put(Alphabet.C , new Transition(Q4,Alphabet.C,Alphabet.C , Alphabet.L));

        Q1.transitions.put(Alphabet.a , new Transition(Q1,Alphabet.a,Alphabet.a , Alphabet.R));
        Q1.transitions.put(Alphabet.b , new Transition(Q1,Alphabet.b,Alphabet.b , Alphabet.R));
        Q1.transitions.put(Alphabet.c , new Transition(Q1,Alphabet.c,Alphabet.c , Alphabet.R));
        Q1.transitions.put(Alphabet.A , new Transition(Q2,Alphabet.A,Alphabet.A , Alphabet.L));
        Q1.transitions.put(Alphabet.B , new Transition(Q2,Alphabet.B,Alphabet.B , Alphabet.L));
        Q1.transitions.put(Alphabet.C , new Transition(Q2,Alphabet.C,Alphabet.C , Alphabet.L));
        Q1.transitions.put(Alphabet.$, new Transition(Q2,Alphabet.$,Alphabet.$, Alphabet.L));

        Q2.transitions.put(Alphabet.a , new Transition(Q3,Alphabet.a,Alphabet.A , Alphabet.L));
        Q2.transitions.put(Alphabet.b , new Transition(Q3,Alphabet.b,Alphabet.B , Alphabet.L));
        Q2.transitions.put(Alphabet.c , new Transition(Q3,Alphabet.c,Alphabet.C , Alphabet.L));

        Q3.transitions.put(Alphabet.a , new Transition(Q3,Alphabet.a,Alphabet.a , Alphabet.L));
        Q3.transitions.put(Alphabet.b , new Transition(Q3,Alphabet.b,Alphabet.b , Alphabet.L));
        Q3.transitions.put(Alphabet.c , new Transition(Q3,Alphabet.c,Alphabet.c , Alphabet.L));
        Q3.transitions.put(Alphabet.A , new Transition(Q0,Alphabet.A,Alphabet.A , Alphabet.R));
        Q3.transitions.put(Alphabet.B , new Transition(Q0,Alphabet.B,Alphabet.B , Alphabet.R));
        Q3.transitions.put(Alphabet.C , new Transition(Q0,Alphabet.C,Alphabet.C , Alphabet.R));

        Q4.transitions.put(Alphabet.A , new Transition(Q14,Alphabet.A,Alphabet.a , Alphabet.R));
        Q4.transitions.put(Alphabet.B , new Transition(Q11,Alphabet.B,Alphabet.b , Alphabet.R));
        Q4.transitions.put(Alphabet.C , new Transition(Q5,Alphabet.C,Alphabet.c , Alphabet.R));

        Q5.transitions.put(Alphabet.a , new Transition(Q5,Alphabet.a,Alphabet.a , Alphabet.R));
        Q5.transitions.put(Alphabet.b , new Transition(Q5,Alphabet.b,Alphabet.b , Alphabet.R));
        Q5.transitions.put(Alphabet.c , new Transition(Q5,Alphabet.c,Alphabet.c , Alphabet.R));
        Q5.transitions.put(Alphabet.A , new Transition(Q6,Alphabet.A,Alphabet.A , Alphabet.R));
        Q5.transitions.put(Alphabet.B , new Transition(Q6,Alphabet.B,Alphabet.B , Alphabet.R));
        Q5.transitions.put(Alphabet.C , new Transition(Q6,Alphabet.C,Alphabet.C , Alphabet.R));

        Q6.transitions.put(Alphabet.a , new Transition(Q7,Alphabet.a,Alphabet.a , Alphabet.L));
        Q6.transitions.put(Alphabet.b , new Transition(Q7,Alphabet.b,Alphabet.b , Alphabet.L));
        Q6.transitions.put(Alphabet.c , new Transition(Q7,Alphabet.c,Alphabet.c , Alphabet.L));
        Q6.transitions.put(Alphabet.A , new Transition(Q6,Alphabet.A,Alphabet.A , Alphabet.R));
        Q6.transitions.put(Alphabet.B , new Transition(Q6,Alphabet.B,Alphabet.B , Alphabet.R));
        Q6.transitions.put(Alphabet.C , new Transition(Q6,Alphabet.C,Alphabet.C , Alphabet.R));
        Q6.transitions.put(Alphabet.$, new Transition(Q7,Alphabet.$,Alphabet.$, Alphabet.L));

        Q7.transitions.put(Alphabet.C , new Transition(Q8,Alphabet.C,Alphabet.c , Alphabet.L));

        Q8.transitions.put(Alphabet.a , new Transition(Q9,Alphabet.a,Alphabet.a , Alphabet.L));
        Q8.transitions.put(Alphabet.b , new Transition(Q9,Alphabet.b,Alphabet.b , Alphabet.L));
        Q8.transitions.put(Alphabet.c , new Transition(Q9,Alphabet.c,Alphabet.c , Alphabet.L));
        Q8.transitions.put(Alphabet.A , new Transition(Q8,Alphabet.A,Alphabet.A , Alphabet.L));
        Q8.transitions.put(Alphabet.B , new Transition(Q8,Alphabet.B,Alphabet.B , Alphabet.L));
        Q8.transitions.put(Alphabet.C , new Transition(Q8,Alphabet.C,Alphabet.C , Alphabet.L));

        Q9.transitions.put(Alphabet.a , new Transition(Q9,Alphabet.a,Alphabet.a , Alphabet.L));
        Q9.transitions.put(Alphabet.b , new Transition(Q9,Alphabet.b,Alphabet.b , Alphabet.L));
        Q9.transitions.put(Alphabet.c , new Transition(Q9,Alphabet.c,Alphabet.c , Alphabet.L));
        Q9.transitions.put(Alphabet.A , new Transition(Q10,Alphabet.A,Alphabet.A , Alphabet.R));
        Q9.transitions.put(Alphabet.B , new Transition(Q10,Alphabet.B,Alphabet.B , Alphabet.R));
        Q9.transitions.put(Alphabet.C , new Transition(Q10,Alphabet.C,Alphabet.C , Alphabet.R));
        Q9.transitions.put(Alphabet.$, new Transition(Q17,Alphabet.$,Alphabet.$, Alphabet.R));


        Q10.transitions.put(Alphabet.a , new Transition(Q4,Alphabet.a,Alphabet.a , Alphabet.L));
        Q10.transitions.put(Alphabet.b , new Transition(Q4,Alphabet.b,Alphabet.b , Alphabet.L));
        Q10.transitions.put(Alphabet.c , new Transition(Q4,Alphabet.c,Alphabet.c , Alphabet.L));

        Q11.transitions.put(Alphabet.a , new Transition(Q11,Alphabet.a,Alphabet.a , Alphabet.R));
        Q11.transitions.put(Alphabet.b , new Transition(Q11,Alphabet.b,Alphabet.b , Alphabet.R));
        Q11.transitions.put(Alphabet.c , new Transition(Q11,Alphabet.c,Alphabet.c , Alphabet.R));
        Q11.transitions.put(Alphabet.A , new Transition(Q12,Alphabet.A,Alphabet.A , Alphabet.R));
        Q11.transitions.put(Alphabet.B , new Transition(Q12,Alphabet.B,Alphabet.B , Alphabet.R));
        Q11.transitions.put(Alphabet.C , new Transition(Q12,Alphabet.C,Alphabet.C , Alphabet.R));

        Q12.transitions.put(Alphabet.a , new Transition(Q13,Alphabet.a,Alphabet.a , Alphabet.L));
        Q12.transitions.put(Alphabet.b , new Transition(Q13,Alphabet.b,Alphabet.b , Alphabet.L));
        Q12.transitions.put(Alphabet.c , new Transition(Q13,Alphabet.c,Alphabet.c , Alphabet.L));
        Q12.transitions.put(Alphabet.A , new Transition(Q12,Alphabet.A,Alphabet.A , Alphabet.R));
        Q12.transitions.put(Alphabet.B , new Transition(Q12,Alphabet.B,Alphabet.B , Alphabet.R));
        Q12.transitions.put(Alphabet.C , new Transition(Q12,Alphabet.C,Alphabet.C , Alphabet.R));
        Q12.transitions.put(Alphabet.$, new Transition(Q13,Alphabet.$,Alphabet.$, Alphabet.L));

        Q13.transitions.put(Alphabet.B , new Transition(Q8,Alphabet.B,Alphabet.b , Alphabet.L));

        Q14.transitions.put(Alphabet.a , new Transition(Q14,Alphabet.a,Alphabet.a , Alphabet.R));
        Q14.transitions.put(Alphabet.b , new Transition(Q14,Alphabet.b,Alphabet.b , Alphabet.R));
        Q14.transitions.put(Alphabet.c , new Transition(Q14,Alphabet.c,Alphabet.c , Alphabet.R));
        Q14.transitions.put(Alphabet.A , new Transition(Q15,Alphabet.A,Alphabet.A , Alphabet.R));
        Q14.transitions.put(Alphabet.B , new Transition(Q15,Alphabet.B,Alphabet.B , Alphabet.R));
        Q14.transitions.put(Alphabet.C , new Transition(Q15,Alphabet.C,Alphabet.C , Alphabet.R));

        Q15.transitions.put(Alphabet.a , new Transition(Q16,Alphabet.a,Alphabet.a , Alphabet.L));
        Q15.transitions.put(Alphabet.b , new Transition(Q16,Alphabet.b,Alphabet.b , Alphabet.L));
        Q15.transitions.put(Alphabet.c , new Transition(Q16,Alphabet.c,Alphabet.c , Alphabet.L));
        Q15.transitions.put(Alphabet.A , new Transition(Q15,Alphabet.A,Alphabet.A , Alphabet.R));
        Q15.transitions.put(Alphabet.B , new Transition(Q15,Alphabet.B,Alphabet.B , Alphabet.R));
        Q15.transitions.put(Alphabet.C , new Transition(Q15,Alphabet.C,Alphabet.C , Alphabet.R));
        Q15.transitions.put(Alphabet.$, new Transition(Q16,Alphabet.$,Alphabet.$, Alphabet.L));

        Q16.transitions.put(Alphabet.A , new Transition(Q8,Alphabet.A,Alphabet.a , Alphabet.L));
    }

    public Alphabet read(){
        if(map.containsKey(tape.charAt(head)))
            return map.get(tape.charAt(head));

        return null;
    }
    public void write(char ch){
        if(map.containsKey(ch)){
            Alphabet alphabet = map.get(ch);
            StringBuilder sb = new StringBuilder(tape);
            sb.setCharAt(head , ch);
            tape = sb.toString();
            return;
        }

        throw new IllegalArgumentException("Wrong alphabet: " + ch);
    }

    public void traverse(){
        State state = Q0;
        while (true) {
            if(state.isFinal)
                break;
            Alphabet read = read();
            Transition transition = state.transitions.get(read);
            if(transition == null)
                break;
            saveTransition(state , transition);
            write(transition.write.name().charAt(0));
            handleTapeCommand(transition.tapeCommand);
            state = transition.to;
        }
        if(state.isFinal){
            System.out.println("accepts!");
            while (!queue.isEmpty()){
                System.out.println(queue.poll().toString());
            }
        }else{
            System.out.println("doesn't accept!");
        }
    }

    private void saveTransition(State from , Transition transition){
        TuringModel model = new TuringModel(
                from.name ,
                transition.to.name ,
                transition.read.name() ,
                transition.write.name() ,
                transition.tapeCommand.name());

        queue.add(model);
    }

    private void handleTapeCommand(Alphabet alphabet){
        switch (alphabet){
            case R:
                this.head++;
                break;
            case L:
                this.head--;
                break;
            default: throw new IllegalArgumentException("Wrong tape command: " + alphabet.name());
        }
    }
}
